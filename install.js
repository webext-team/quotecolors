const APP_DISPLAY_NAME = "Quote Colors";
const APP_NAME = "quotecolors";
const APP_PACKAGE = "/quotecolors";
const APP_VERSION = "0.3";

const APP_JAR_FILE = "quotecolors.jar";
const APP_PREFS_FILE = "defaults/preferences/quotecolors.js";
const APP_CONTENT_FOLDER = "content/quotecolors/";
const APP_LOCALES = [ "en-US", "de-DE", "es-ES", "fr-FR", "it-IT", "ja-JP", "pl-PL", "cs-CZ", "hu-HU", "ru-RU", "da-DK" ];

const APP_SUCCESS_MESSAGE = "You need to restart Mozilla first.";
const INST_TO_PROFILE = "Do you wish to install "+APP_DISPLAY_NAME+" to your profile?\nThis will mean it does not need reinstalling when you update Mozilla.\n(Click Cancel if you want "+APP_DISPLAY_NAME+" installing to the Mozilla directory.)";

initInstall(APP_NAME, APP_PACKAGE, APP_VERSION);

// profile installs only work since 2003-03-06
var instToProfile = confirm(INST_TO_PROFILE);

var chromefolder = instToProfile ? getFolder("Profile", "chrome") : getFolder("chrome");
var profileflag = instToProfile ? PROFILE_CHROME : DELAYED_CHROME;

var err = addFile(APP_PACKAGE, APP_VERSION, "chrome/"+APP_JAR_FILE, chromefolder, null)

if(err == SUCCESS) {
	var jar = getFolder(chromefolder, APP_JAR_FILE);

  addFile(APP_NAME, APP_PREFS_FILE, getFolder("Program", "defaults/pref/"), null);

	registerChrome(CONTENT | profileflag, jar, APP_CONTENT_FOLDER);

  for(var i in APP_LOCALES) {
    registerChrome(LOCALE  | profileflag, jar, "locale/"+APP_LOCALES[i]+"/"+APP_NAME+"/");
  }

	err = performInstall();
	if(err == SUCCESS || err == 999) {
		alert(APP_DISPLAY_NAME+" "+APP_VERSION+" has been succesfully installed.\n"+APP_SUCCESS_MESSAGE);
	} else {
		alert("Install failed. Error code:" + err);
		cancelInstall(err);
	}
} else {
	alert("Failed to create " +APP_JAR_FILE +"\n"
		+"You probably don't have appropriate permissions \n"
		+"(write access to your profile or chrome directory). \n"
		+"_____________________________\nError code:" + err);
	cancelInstall(err);
}
