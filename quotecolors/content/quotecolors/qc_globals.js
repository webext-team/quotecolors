// border widths in unit "ex" (correspond to "thin", "medium" and "thick")
const aQC_borderwidth = [0.1667, 0.5, 0.8333];
// border styles
const aQC_borderstyle = ["double", "solid", "dashed", "dotted"];
// maximum quote levels
const nQC_MAX_LEVELS = 5;
// topic of quote colors pref change observer
const sQCPrefUpdateTopic = "qc-pref-update";
