var colors_fg = new Array(nQC_MAX_LEVELS);
var colors_bg = new Array(nQC_MAX_LEVELS);

function checkOptions()
{
  var graphicalquoting = document.getElementById("graphicalquoting").checked;
  var idx_borderstyle = parseInt(document.getElementById("borderstyle").value);
  var idx_borderwidth = parseInt(document.getElementById("borderwidth").value);

  // borderwidth "thin" and borderstyle "double" don't work together
  if(graphicalquoting && idx_borderstyle==0 && idx_borderwidth==0)
  {
    var bundleSettings = document.getElementById("bundle_qcsettings");
    if(! window.confirm(bundleSettings.getString("confirmThinDoubleBorder")))
      return false;
  }

  savePrefs();
  
  var observerService = Components.classes["@mozilla.org/observer-service;1"]
                                  .getService(Components.interfaces.nsIObserverService);
  observerService.notifyObservers(null, sQCPrefUpdateTopic, null);

  return true;
}


function updatePreview()
{
  var colortext = document.getElementById("colortext").checked;
  var colorbg = document.getElementById("colorbackground").checked;
  var graphicalquoting = document.getElementById("graphicalquoting").checked;

  var borderwidth, bordercolor, staticbordercolor, borderstyletop, borderstyleright;
  var borderstylebottom, borderstyleleft, borderstyle, collapseborders;
  if(graphicalquoting)
  {
    staticbordercolor = parseInt(document.getElementById("bordermode").value);
    if(staticbordercolor)
      bordercolor = document.getElementById("bordercolor").getAttribute("color");

    var index_borderstyle = parseInt(document.getElementById("borderstyle").value);
    borderstyle = aQC_borderstyle[index_borderstyle];
    var index_borderwidth = parseInt(document.getElementById("borderwidth").value);
    borderwidth = aQC_borderwidth[index_borderwidth];

    borderstyletop = document.getElementById("showbordertop").checked ? borderstyle : "none";
    borderstyleright = document.getElementById("showborderright").checked ? borderstyle : "none";
    borderstylebottom = document.getElementById("showborderbottom").checked ? borderstyle : "none";
    borderstyleleft = document.getElementById("showborderleft").checked ? borderstyle : "none";

  }
  collapseborders = document.getElementById("collapseborders").checked;

  for(var i=0; i<nQC_MAX_LEVELS; i++)
  {
    var cur_elm = document.getElementById("blocklevel"+(i+1));
    cur_elm.style.color = ( colortext ? colors_fg[i] : "inherit" );
    cur_elm.style.backgroundColor = ( colorbg ? colors_bg[i] : "inherit" );

    var cur_qcelm = document.getElementById("qchar"+(i+1));
    cur_qcelm.style.display = "inline";

    if(!collapseborders) cur_elm.style.padding = "0ex 2ex 1ex 2ex";
    cur_elm.style.margin="0.5em 0em 0em 0em";

    if(graphicalquoting)
    {
      cur_elm.style.borderColor = ( staticbordercolor ? bordercolor : colors_fg[i] );
      cur_elm.style.borderWidth = borderwidth +"ex";
      cur_elm.style.borderStyle = borderstyletop +" "+ borderstyleright +" "+ borderstylebottom +" "+ borderstyleleft;
      
      cur_qcelm.style.display = "none";
      if(i==0) cur_elm.style.marginTop = "0em";

      if(collapseborders)
      {
        if(i>0)
        {
            var leftmargin = (borderstyleleft=="none") ? 2.0 : 2.0 + borderwidth;
            var rightmargin = (borderstyleright=="none") ? 2.0 : 2.0 + borderwidth;
            cur_elm.style.marginLeft="-"+leftmargin+"ex";
            cur_elm.style.marginRight="-"+rightmargin+"ex";
        }
        cur_elm.style.padding = "0ex 2ex 0ex 2ex";
      }
    }
    else
    {
      cur_elm.style.borderStyle ="none";
      cur_elm.style.padding = "0.5em 0em 0em 0em";
      if(i==0) cur_elm.style.marginTop = "1.5em";
    }
  }
  if(!graphicalquoting || collapseborders)
    document.getElementById("blocklevel5").style.paddingBottom = "0.5em";

}

function updatePreviewTextAndBg()
{
  if(document.getElementById("usermsgcolors").checked)
  {
    var msgtextcolor = document.getElementById("msgtextcolor").getAttribute("color");
    var msgbgcolor = document.getElementById("msgbgcolor").getAttribute("color");
  }
  else
  {
    var msgtextcolor = pref.getCharPref("browser.display.foreground_color");  
    var msgbgcolor = pref.getCharPref("browser.display.background_color");
  }
  document.getElementById("previewbox").style.color = msgtextcolor;
  document.getElementById("previewbox").style.backgroundColor = msgbgcolor;
}

// taken from editor, using some ugly modifications. no, it isn't a good solution...

function GetColorAndUpdate(ColorWellID)
{
  var colorWell = document.getElementById(ColorWellID);
  if (!colorWell) return;

  // Don't allow a blank color, i.e., using the "default"
  var colorObj = { NoDefault:true, Type:"", TextColor:0, PageColor:0, Cancel:false };

  switch( ColorWellID )
  {
    case "level1fgCW":
      colorObj.Type = "Text";
      colorObj.TextColor = colors_fg[0];
      break;
    case "level1bgCW":
      colorObj.Type = "Page";
      colorObj.PageColor = colors_bg[0];
      break;
    case "level2fgCW":
      colorObj.Type = "Text";
      colorObj.TextColor = colors_fg[1];
      break;
    case "level2bgCW":
      colorObj.Type = "Page";
      colorObj.PageColor = colors_bg[1];
      break;
    case "level3fgCW":
      colorObj.Type = "Text";
      colorObj.TextColor = colors_fg[2];
      break;
    case "level3bgCW":
      colorObj.Type = "Page";
      colorObj.PageColor = colors_bg[2];
      break;
    case "level4fgCW":
      colorObj.Type = "Text";
      colorObj.TextColor = colors_fg[3];
      break;
    case "level4bgCW":
      colorObj.Type = "Page";
      colorObj.PageColor = colors_bg[3];
      break;
    case "level5fgCW":
      colorObj.Type = "Text";
      colorObj.TextColor = colors_fg[4];
      break;
    case "level5bgCW":
      colorObj.Type = "Page";
      colorObj.PageColor = colors_bg[4];
      break;
  }

  window.openDialog("chrome://editor/content/EdColorPicker.xul", "_blank", "chrome,close,titlebar,modal", "", colorObj);

  // User canceled the dialog
  if (colorObj.Cancel)
    return;

  var color = "";
  switch( ColorWellID )
  {
    case "level1fgCW":
      color = colors_fg[0] = colorObj.TextColor;
      break;
    case "level1bgCW":
      color = colors_bg[0] = colorObj.BackgroundColor;
      break;
    case "level2fgCW":
      color = colors_fg[1] = colorObj.TextColor;
      break;
    case "level2bgCW":
      color = colors_bg[1] = colorObj.BackgroundColor;
      break;
    case "level3fgCW":
      color = colors_fg[2] = colorObj.TextColor;
      break;
    case "level3bgCW":
      color = colors_bg[2] = colorObj.BackgroundColor;
      break;
    case "level4fgCW":
      color = colors_fg[3] = colorObj.TextColor;
      break;
    case "level4bgCW":
      color = colors_bg[3] = colorObj.BackgroundColor;
      break;
    case "level5fgCW":
      color = colors_fg[4] = colorObj.TextColor;
      break;
    case "level5bgCW":
      color = colors_bg[4] = colorObj.BackgroundColor;
      break;
  }

  setColorWell(ColorWellID, color); 
 
  updatePreview();
}


function setColorWell(ColorWellID, color)
{
  var colorWell = document.getElementById(ColorWellID);
  if (colorWell)
  {
    if (color)
    {
      colorWell.setAttribute("style", "background-color:"+color);
    }
  }
}


function setDefaultColors()
{
  for(var f=0; f<nQC_MAX_LEVELS; f++)
  {
      pref.lockPref("quotecolors.fg.l"+(f+1));
      colors_fg[f] = pref.getCharPref( "quotecolors.fg.l"+(f+1) );
      pref.unlockPref("quotecolors.fg.l"+(f+1));
      setColorWell("level"+(f+1)+"fgCW", colors_fg[f]);

      pref.lockPref("quotecolors.bg.l"+(f+1));
      colors_bg[f] = pref.getCharPref( "quotecolors.bg.l"+(f+1) );
      pref.unlockPref("quotecolors.bg.l"+(f+1));
      setColorWell("level"+(f+1)+"bgCW", colors_bg[f]);
  }

  updatePreview();
}


function toggleGraphicalQuoting(enable)
{
  if(enable)
    document.getElementById("bc-gqenabled").removeAttribute("disabled");
  else
    document.getElementById("bc-gqenabled").setAttribute("disabled", true);
}

function toggleShowStructs(enable)
{
  document.getElementById("bc-showstructs").setAttribute("disabled", !enable);
}

function toggleUserMsgColors(enable)
{
  document.getElementById("bc-usermsgcolors").setAttribute("disabled", !enable);
}