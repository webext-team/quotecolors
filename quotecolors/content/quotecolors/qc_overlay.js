
var gbQCPrintMode = false;    // are we in printing mode?
var gnPrintMsgCnt = -1;

var QCPrefUpdateObserver = {
  register: function() {
    this.oObsServ = Components.classes["@mozilla.org/observer-service;1"]
                              .getService(Components.interfaces.nsIObserverService);
    this.oObsServ.addObserver(this, sQCPrefUpdateTopic, false);
  },
  
  unregister: function() {
    if(this.oObsServ)
      this.oObsServ.removeObserver(this, sQCPrefUpdateTopic);
  },
  
  observe: function(subject, topic, data) {
    if(topic == sQCPrefUpdateTopic) {
      QCObj.getQCPrefs();
      if(gDBView) ReloadMessage();
    }
  }
};

var QCMailPaneConfigObserver = {
  sPrefName : "mail.pane_config.dynamic", // pref name to observe
  
  register: function() {
    this.oPrefServ = Components.classes["@mozilla.org/preferences-service;1"]
                               .getService(Components.interfaces.nsIPrefBranch);
    this.oPrefServ.QueryInterface(Components.interfaces.nsIPrefBranchInternal);
    this.oPrefServ.addObserver(this.sPrefName, this, false);
  },
  
  unregister: function() {
    if(this.oPrefServ)
      this.oPrefServ.removeObserver(this.sPrefName, this);
  },
  
  observe: function(subject, topic, data) {
    // re-initialize message browser reference when mail pane config pref changes
    if(topic == "nsPref:changed")
      QCObj.initBrowserRef();
  }
};

function QCunloadMessenger()
{
  QCMailPaneConfigObserver.unregister();
  QCPrefUpdateObserver.unregister();
}

function QCunloadMsgWnd()
{
  QCPrefUpdateObserver.unregister();
}