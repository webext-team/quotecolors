var QCObj = {

  // are we running Thunderbird 3 beta 3 or later?
  // If yes, we need to use a modified way of retrieving the message URI
  bAppIsTb3 : false,
  // are we running in Seamonkey 2 or later?
  bAppIsSM2 : false,
  
  // reference to message browser
  oMsgBrowser : null,
  
  // properties of current loaded message
  bIsHTMLMessage : false,
  bIsFormatFlowed : false,
  sLoadedMsgURI : '',
  
  objnsIPrefBranch : Components.classes["@mozilla.org/preferences-service;1"]
                               .getService(Components.interfaces.nsIPrefBranch),
  
  objnsIMessenger : Components.classes["@mozilla.org/messenger;1"]
                              .getService(Components.interfaces.nsIMessenger),
  
  objnsIXULAppInfo : Components.classes["@mozilla.org/xre/app-info;1"]
                               .getService(Components.interfaces.nsIXULAppInfo),
  objnsIVersionComparator : Components.classes["@mozilla.org/xpcom/version-comparator;1"]
                                      .getService(Components.interfaces.nsIVersionComparator),
  
  aPrefColorsFg : new Array(nQC_MAX_LEVELS),
  aPrefColorsBg : new Array(nQC_MAX_LEVELS),
  bHideStructDelim : false,
  
  
  // ########################################################################
  // basic object initialization (get application/version, read prefs, init message browser)
  // returns: void
  
  initMain : function()
  {
  
    if(QCObj.objnsIXULAppInfo.ID == "{3550f703-e582-4d05-9a08-453d09bdfdc6}")
    {
      if(QCObj.objnsIVersionComparator.compare(QCObj.objnsIXULAppInfo.version, "3.0b3") >= 0) {
        QCObj.bAppIsTb3 = true;
      }
    }
    else if(QCObj.objnsIXULAppInfo.ID == "{92650c4d-4b8e-4d2a-b7eb-24ecf4f6b63a}")
    {
      if(QCObj.objnsIVersionComparator.compare(QCObj.objnsIXULAppInfo.version, "2.0a1") >= 0) {
        QCObj.bAppIsSM2 = true;
      }
    }
    // get preferences
    QCObj.getQCPrefs();
    // init message browser
    QCObj.initBrowserRef();
  
  },
  
  // ########################################################################
  // initialize message browser reference 'oMsgBrowser' and add 'load' event listener
  // returns: void
  
  initBrowserRef : function()
  {
  
    if(gbQCPrintMode) {
      this.oMsgBrowser = document.getElementById("content");
    }
    else {
      this.oMsgBrowser = document.getElementById("messagepane");
    }
    
    if(this.oMsgBrowser)
      this.oMsgBrowser.addEventListener("load", this.applyColorsToMsg, true);
  
  },
  
  // ########################################################################
  // read Quote Colors preferences
  // returns: void
  
  getQCPrefs : function()
  {

    // populate text and background color arrays for all levels
    for(var i=0; i<nQC_MAX_LEVELS; i++)
    {
      this.aPrefColorsFg[i] = this.objnsIPrefBranch.getCharPref("quotecolors.fg.l" + (i+1));
      this.aPrefColorsBg[i] = this.objnsIPrefBranch.getCharPref("quotecolors.bg.l" + (i+1));
    }
    this.bPrefColorText = this.objnsIPrefBranch.getBoolPref("quotecolors.colorText");
    this.bPrefColorBg = this.objnsIPrefBranch.getBoolPref("quotecolors.colorBackground");
    this.sPrefBorderColor = this.objnsIPrefBranch.getCharPref("quotecolors.bordercolor");
    var nIdxBorderWidth = this.objnsIPrefBranch.getIntPref("quotecolors.borderwidth");
    this.nBorderWidth = aQC_borderwidth[nIdxBorderWidth];
    this.nPrefBorderMode = this.objnsIPrefBranch.getIntPref("quotecolors.borderMode");
    var nIdxBorderStyle = this.objnsIPrefBranch.getIntPref("quotecolors.borderstyle");
    var sBorderStyle = aQC_borderstyle[nIdxBorderStyle];
    this.bPrefBorderLeftEn = this.objnsIPrefBranch.getBoolPref("quotecolors.borderposition.left");
    this.bPrefBorderRightEn = this.objnsIPrefBranch.getBoolPref("quotecolors.borderposition.right");
    this.sBorderLeftStyle = this.bPrefBorderLeftEn ? sBorderStyle : "none";
    this.sBorderRightStyle = this.bPrefBorderRightEn ? sBorderStyle : "none";
    this.sBorderTopStyle = this.objnsIPrefBranch.getBoolPref("quotecolors.borderposition.top") ? sBorderStyle : "none";
    this.sBorderBottomStyle = this.objnsIPrefBranch.getBoolPref("quotecolors.borderposition.bottom") ? sBorderStyle : "none";
    this.bPrefCollapseBorders = this.objnsIPrefBranch.getBoolPref("quotecolors.collapseBorders");
    this.bPrefColorHTMLMsg = this.objnsIPrefBranch.getBoolPref("quotecolors.colorHTMLmessages");

    this.bPrefUseCustomMsgColors = this.objnsIPrefBranch.getBoolPref("quotecolors.usermsgcolors");
    this.sPrefMsgTextColor = this.objnsIPrefBranch.getCharPref("quotecolors.messagetextcolor");
    this.sPrefMsgBgColor = this.objnsIPrefBranch.getCharPref("quotecolors.messagebgcolor");
    this.sPrefMsgLinkColor = this.objnsIPrefBranch.getCharPref("quotecolors.messagelinkcolor");
    this.sPrefMsgLinkHoverColor = this.objnsIPrefBranch.getCharPref("quotecolors.messagelinkhovercolor");
    this.sPrefSigColor = this.objnsIPrefBranch.getCharPref("quotecolors.signaturecolor");
    this.sPrefSigLinkColor = this.objnsIPrefBranch.getCharPref("quotecolors.signaturelinkcolor");
    this.bPrefHideSignatures = this.objnsIPrefBranch.getBoolPref("quotecolors.hidesignatures");
    this.bPrefHideStructDelim = this.objnsIPrefBranch.getBoolPref("quotecolors.hidestructdelimiters");

  },

  // ########################################################################
  // generate CSS that will be inserted into mail message
  // returns: string
  
  generateStyleBlock : function(bMsgContainsQuotes, bMsgIsJunk)
  {

    const sBqSelector = "blockquote[type=cite] ";
    const sBqSelector5 = sBqSelector + sBqSelector + sBqSelector + sBqSelector + sBqSelector;
    
    var sStyleBlock = '';
    
    if(bMsgContainsQuotes)
    {
      if(!bMsgIsJunk)
      {
        if( (this.bIsHTMLMessage && this.bPrefColorHTMLMsg) || !this.bIsHTMLMessage)
        {
        
          for(var i=0; i<nQC_MAX_LEVELS; i++)
          {
            var sBqSelectorLevel = '';
            for(var j=0; j<=i; j++) {
              sBqSelectorLevel += sBqSelector;
            }
            
            sStyleBlock += sBqSelectorLevel + ", " + sBqSelector5 + sBqSelectorLevel;
            sStyleBlock += "{";
            
            // text color
            sStyleBlock += "color:" + ( this.bPrefColorText ? this.aPrefColorsFg[i] : "inherit" ) + " !important;";
            
            // background color
            if(this.bPrefColorBg)
              sStyleBlock += "background-color:" + this.aPrefColorsBg[i] + " !important;";
            
            // only add/style borders if graphical quoting is enabled
            if(this.bGraphQuotEn)
            {
              sStyleBlock += "border-color:" +
                             ( (this.nPrefBorderMode==0) ? this.aPrefColorsFg[i] : this.sPrefBorderColor )
                             + " !important;";
              sStyleBlock += "border-width:"+this.nBorderWidth+"ex !important;"
              sStyleBlock += "border-style:"+this.sBorderTopStyle+" "+this.sBorderRightStyle
                              +" "+this.sBorderBottomStyle+" "+this.sBorderLeftStyle+" !important;";
              
              if(this.bPrefCollapseBorders)
              {
                if(i>0)
                {
                  var leftmargin = !this.bPrefBorderLeftEn ? 2.0 : 2.0 + this.nBorderWidth;
                  var rightmargin = !this.bPrefBorderRightEn ? 2.0 : 2.0 + this.nBorderWidth;
                  sStyleBlock += "margin-left:-"+leftmargin+"ex;margin-right:-"+rightmargin+"ex;";
                }
                else
                  sStyleBlock += "padding:0ex 2ex 0ex 2ex !important;";
              }
              else
              {
                // Every supported application/version uses slightly different paddings for blockquotes.
                // The goal here is to create a similar appearance across the different apps, i.e.:
                //    * the left and right paddings should be equal
                //    * they should not be too small (< 1em)
                //    * bottom padding should be greater than zero
                // All this is done for improved visual differentiation of quote levels. Of course,
                // this also depends on personal taste.
                if(this.bAppIsTb3) // Thunderbird 3 (greater than beta2)
                  sStyleBlock += "padding-left:2ex;padding-right:2ex;padding-bottom:1ex;padding-top:0;";
                else if(this.bAppIsSM2) // SeaMonkey 2
                  sStyleBlock += "padding-left:1em;padding-right:1em;padding-bottom:0.5em;";
                else // Thunderbird 2 and everything else...
                  sStyleBlock += "padding-left:1em !important;padding-right:1em !important;";
              }

            }
            else // non-graphical quoting
            {
              sStyleBlock += "border:none !important;";
              sStyleBlock += "padding:0em !important;";
            }
            
            sStyleBlock += "}\n";
          } // for loop

          if(this.bPrefCollapseBorders)
            sStyleBlock += "blockquote[type=cite] pre{margin-left:0em !important;margin-right:0em !important;}\n";
        }
      
      }
      else // message is marked as junk
      {
        if(!this.bIsFormatFlowed)
        {
          // in junk messages, hide the (default) vertical bars and display ">" characters instead
          sStyleBlock += sBqSelector + "{border:none !important;padding:0em !important;}\n";
          sStyleBlock += ".moz-txt-citetags {display:inline !important;}\n";
        }
      }
    }

    // set other messages styles (if enabled)
    if(this.bPrefUseCustomMsgColors)
    {
      sStyleBlock += "a:link {color: "+this.sPrefMsgLinkColor+";}\n";
      sStyleBlock += "a:link:hover {color: "+this.sPrefMsgLinkHoverColor+";}\n";
      
      if(!this.bPrefHideSignatures)
      {
        sStyleBlock += ".moz-txt-sig, .moz-signature {color: "+this.sPrefSigColor+";}\n";
        sStyleBlock += ".moz-txt-sig > a, .moz-signature > a {color: "+this.sPrefSigLinkColor+";}\n";
      }
    }

    if(this.bPrefHideSignatures)
      sStyleBlock += ".moz-txt-sig, .moz-signature {display:none;}\n";

    if(this.bHideStructDelim)
      sStyleBlock += ".moz-txt-tag {display:none !important;}\n";

    return sStyleBlock;
  },
  
  // ########################################################################
  // check if currently loaded message is marked as junk
  // returns: boolean
  
  isJunk : function()
  {
    // exclude separately opened *.eml messages
    if( !(/type=application\/x-message-display/.test(this.sLoadedMsgURI)) )
    {
      var msgHdr = this.objnsIMessenger.messageServiceFromURI(this.sLoadedMsgURI)
                                       .messageURIToMsgHdr(this.sLoadedMsgURI);
      var junkScore = msgHdr.getStringProperty("junkscore");
      
      return ((junkScore != "") && (junkScore != "0"));
    }
    return false;
  },
  
  // ########################################################################
  // event listener for "load" events of message browser
  // performs several checks if current message is valid and can be colorized,
  // eventually adds a <style> element with all style information
  // returns: void
  
  applyColorsToMsg : function()
  {

    if(!QCObj.oMsgBrowser)
      return;
    
    // get the message URI of currently loaded message
    if(gbQCPrintMode)
    {
      if(gnPrintMsgCnt == -1) // discard the first call in printing mode
      {
        gnPrintMsgCnt = 0;
        return;
      }
      
      if(window.arguments && window.arguments[0] != null)
      {
        // argument 0 contains number of selected messages (printing multiple messages at once is possible)
        // argument 1 contains array of selected message uris
        var nNumSelMsg = window.arguments[0]; // number of selected messages
        var nIdxCurMsg = (gnPrintMsgCnt > nNumSelMsg-1) ? nNumSelMsg-1 : gnPrintMsgCnt;

        if(window.arguments[1][nIdxCurMsg] != null)
          QCObj.sLoadedMsgURI = window.arguments[1][nIdxCurMsg];
      }
      gnPrintMsgCnt++;
    
    }
    else // not in printing mode
    {
      if(QCObj.bAppIsTb3) { // special treatment for Thunderbird 3
        if(gMessageDisplay && gMessageDisplay.folderDisplay.selectedMessageUris)
          QCObj.sLoadedMsgURI = gMessageDisplay.folderDisplay.selectedMessageUris[0];
      }
      else { // all other supported applications (Thunderbird 2, SeaMonkey 2, ...)
        QCObj.sLoadedMsgURI = GetLoadedMessage();
      }
      
    }

    if(!QCObj.sLoadedMsgURI) return;
    
    // check if a message was loaded that we can colorize (this excludes addressbook cards, for example)
    var messagePrefix = /^mailbox-message:|^imap-message:|^news-message:|^file:/i;
    if( !messagePrefix.test(QCObj.sLoadedMsgURI) ) return;

    var oMsgDoc = QCObj.oMsgBrowser.contentDocument;
    if(!oMsgDoc) return;

    var elmBody = oMsgDoc.getElementsByTagName("body").item(0);
    // does not seem to be a valid message
    if(!elmBody) return;

    var elmDiv = null;
    var nextElm = elmBody.firstChild;
    while( nextElm )
    {
      if( nextElm.nodeName == "DIV" && nextElm.hasAttribute("class") )
      {
        elmDiv = nextElm;
        break;
      }
      nextElm = nextElm.nextSibling;
    }

    if(!elmDiv)
    {
      // empty message, only set background color
      if(QCObj.bPrefUseCustomMsgColors)
        elmBody.bgColor = QCObj.sPrefMsgBgColor;
      return;
    }

    // determine message type: HTML or plain text (flowed or fixed)
    switch( elmDiv.getAttribute("class") )
    {
      case "moz-text-html":
        QCObj.bIsHTMLMessage = true;
        QCObj.bIsFormatFlowed = false;
        break;
      case "moz-text-plain":
        QCObj.bIsHTMLMessage = false;
        QCObj.bIsFormatFlowed = false;
        break;
      case "moz-text-flowed":
        QCObj.bIsHTMLMessage = false;
        QCObj.bIsFormatFlowed = true;
        break;
      default:
        return;
    }

    // check if message is marked as junk
    var bmsgisJunk = QCObj.isJunk();

    // check if there are any quotes in the message
    var bmsgcontainsquotes = false;
    if( oMsgDoc.getElementsByTagName("blockquote").item(0) )
      bmsgcontainsquotes = true;

    // is graphical quoting active?
    QCObj.bGraphQuotEn =
      ( QCObj.bIsHTMLMessage
        || QCObj.objnsIPrefBranch.getBoolPref("mail.quoted_graphical")
        || (!QCObj.objnsIPrefBranch.getBoolPref("mailnews.display.disable_format_flowed_support")
            && QCObj.bIsFormatFlowed)
      );
    
    // are struct delimiters enabled (MailNews option)?
    if( QCObj.bPrefHideStructDelim && QCObj.objnsIPrefBranch.getBoolPref("mail.display_struct") )
    {
      QCObj.bHideStructDelim = true;
    }
    
    // set text and background colors if enabled
    if(QCObj.bPrefUseCustomMsgColors)
    {
      var bgset = elmBody.hasAttribute("bgcolor");
      var textset = elmBody.hasAttribute("text");

      if(!gbQCPrintMode)
      {
        if(!bgset && !textset)
        {
          elmBody.bgColor = QCObj.sPrefMsgBgColor;
          elmBody.text = QCObj.sPrefMsgTextColor;
        }
      }
      else
      {
        var htmltextcolor = QCObj.sPrefMsgTextColor;
        if(textset)
        {
          htmltextcolor = elmBody.getAttribute("text");
          elmBody.removeAttribute("text");
        }
        elmDiv.style.color = htmltextcolor;
      }
    }
    
    // generate the style block, create a new style element
    // and finally add it to the "head" of message
    
    var StyleElement = oMsgDoc.createElement("style");
    StyleElement.type = "text/css";

    var sStyleContent = QCObj.generateStyleBlock(bmsgcontainsquotes, bmsgisJunk);
    var oStyleTextNode = oMsgDoc.createTextNode(sStyleContent);
    StyleElement.appendChild(oStyleTextNode);

    oMsgDoc.getElementsByTagName("head")[0].appendChild(StyleElement);
    
  }

};
